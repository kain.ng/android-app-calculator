package com.example.lab1_calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class CalculateActivity extends AppCompatActivity {
    //ArrayList<Integer> inputs = new ArrayList<Integer>();
    float mParam1, mParam2, mResult;
    TextView mTextbox;
    Button mButton0, mButton1, mButton2, mButton3, mButton4, mButton5, mButton6, mButton7, mButton8, mButton9;
    Button mAddButton, mMinusButton, mMulButton, mDivButton, mEqualButton, mAllClearButton;
    enum ArithmeticOperation {
        ADD,
        SUBTRACT,
        MULIPLY,
        DIVIDE,
    }
    ArithmeticOperation mathOperation;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculate);

        /* No action listener for the textbox. It is used to display results. */
        mTextbox = (TextView) findViewById(R.id.result_textView);
        mTextbox.setText(""); // !! Do this otherwise there will be an extra " " in the textbox

        mButton0 = (Button)findViewById(R.id.digit_0_button);
        mButton0.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                mTextbox.setText(mTextbox.getText() + "0");
            }
        });

        mButton1 = (Button)findViewById(R.id.digit_1_button);
        mButton1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                mTextbox.setText(mTextbox.getText() + "1");
            }
        });

        mButton2 = (Button)findViewById(R.id.digit_2_button);
        mButton2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                mTextbox.setText(mTextbox.getText() + "2");
            }
        });

        mButton3 = (Button)findViewById(R.id.digit_3_button);
        mButton3.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                mTextbox.setText(mTextbox.getText() + "3");
            }
        });

        mButton4 = (Button)findViewById(R.id.digit_4_button);
        mButton4.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                mTextbox.setText(mTextbox.getText() + "4");
            }
        });

        mButton5 = (Button)findViewById(R.id.digit_5_button);
        mButton5.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                mTextbox.setText(mTextbox.getText() + "5");
            }
        });

        mButton6 = (Button)findViewById(R.id.digit_6_button);
        mButton6.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                mTextbox.setText(mTextbox.getText() + "6");
            }
        });

        mButton7 = (Button)findViewById(R.id.digit_7_button);
        mButton7.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                mTextbox.setText(mTextbox.getText() + "7");
            }
        });

        mButton8 = (Button)findViewById(R.id.digit_8_button);
        mButton8.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                mTextbox.setText(mTextbox.getText() + "8");
            }
        });

        mButton9 = (Button)findViewById(R.id.digit_9_button);
        mButton9.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                mTextbox.setText(mTextbox.getText() + "9");
            }
        });


        mAddButton = (Button)findViewById(R.id.add_button);
        mAddButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
              //test by giving it a toast
              Toast.makeText(CalculateActivity.this, R.string.add_toast, Toast.LENGTH_SHORT).show();

              //do the add operation
              if (mTextbox.getText().length() != 0) {
                  mathOperation = ArithmeticOperation.ADD;                  // record ADD operation
                  String s = mTextbox.getText().toString();                 // get the int value from textbox and put it to param1
                  mParam1 = Float.parseFloat(s);
                  mTextbox.setText("");                                     // empty the textbox
              }
            }
        });

        mMinusButton = (Button)findViewById(R.id.minus_button);
        mMinusButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //test by giving it a toast
                Toast.makeText(CalculateActivity.this, R.string.minus_toast, Toast.LENGTH_SHORT).show();

                //do the add operation
                if (mTextbox.getText().length() != 0) {
                    mathOperation = ArithmeticOperation.SUBTRACT;                  // record ADD operation
                    String s = mTextbox.getText().toString();                 // get the int value from textbox and put it to param1
                    mParam1 = Float.parseFloat(s);
                    mTextbox.setText("");                                     // empty the textbox
                }
            }
        });

        mMulButton = (Button)findViewById(R.id.mul_button);
        mMulButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //test by giving it a toast
                Toast.makeText(CalculateActivity.this, R.string.mul_toast, Toast.LENGTH_SHORT).show();

                //do the add operation
                if (mTextbox.getText().length() != 0) {
                    mathOperation = ArithmeticOperation.MULIPLY;                  // record ADD operation
                    String s = mTextbox.getText().toString();                 // get the int value from textbox and put it to param1
                    mParam1 = Float.parseFloat(s);
                    mTextbox.setText("");                                     // empty the textbox
                }
            }
        });

        mDivButton = (Button)findViewById(R.id.div_button);
        mDivButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //test by giving it a toast
                Toast.makeText(CalculateActivity.this, R.string.div_toast, Toast.LENGTH_SHORT).show();

                //do the add operation
                if (mTextbox.getText().length() != 0) {
                    mathOperation = ArithmeticOperation.DIVIDE;                  // record ADD operation
                    String s = mTextbox.getText().toString();                 // get the int value from textbox and put it to param1
                    mParam1 = Float.parseFloat(s);
                    mTextbox.setText("");                                     // empty the textbox
                }
            }
        });

        mEqualButton = (Button) findViewById(R.id.equal_button);
        mEqualButton.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mTextbox.getText().length() != 0){
                    mParam2 = Float.parseFloat(mTextbox.getText() + ""); //get the int value from textbox and put it to param2
                    Log.i("Kain --", "param2: ["+mParam2+"]", null);

                    switch(mathOperation) {
                        case ADD:
                        mResult = mParam1 + mParam2;
                        mTextbox.setText(String.valueOf(mResult));
                        mParam1 = mResult;                  //allow next math operation
                        break;

                        case SUBTRACT:
                        mResult = mParam1 - mParam2;
                        mTextbox.setText(String.valueOf(mResult));
                        mParam1 = mResult;                  //allow next math operation
                        break;

                        case MULIPLY:
                        mResult = mParam1 * mParam2;
                        mTextbox.setText(String.valueOf(mResult));
                        mParam1 = mResult;                  //allow next math operation
                        break;

                        case DIVIDE:
                        mResult = mParam1 / mParam2;
                        mTextbox.setText(String.valueOf(mResult));
                        mParam1 = mResult;                  //allow next math operation
                        break;

                        default:
                        Log.e("Equal switch case -- default case happened. Check for the bug", null, null);
                    }
                }
            }
        }));

        mAllClearButton = (Button) findViewById(R.id.ac_button);
        mAllClearButton.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTextbox.setText("");
                mParam1 = mParam2 = mResult = 0; /* reset value of mParam1 & mParam2 */
                Log.i("Kain --", "result: ["+mResult+"]", null);
                Log.i("Kain --", "p1: ["+mParam1+"]", null);
                Log.i("Kain --", "p2: ["+mParam2+"]", null);
            }
        }));
    }
}